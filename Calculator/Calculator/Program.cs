﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculator calculator = new Calculator();
            Operation operation = Operation.add;
            String operationToRemember;
            String readInput = "s";
            double lastNumber = 0;
            do
            {


                if (calculator.verifyIfValidOperation(readInput) != 1)
                {
                    while (calculator.verifyIfIsNumber(readInput) != true)
                    {
                        Console.WriteLine("Enter the first number");
                        readInput = Console.ReadLine();
                        if (calculator.verifyIfIsNumber(readInput) == true)
                            calculator.firstNumber = Convert.ToDouble(readInput);
                        else
                            Console.WriteLine("Not a number try again");
                    }
                    readInput = "s";
                    while (calculator.verifyIfIsNumber(readInput) != true)
                    {
                        Console.WriteLine("Enter the second number");
                        readInput = Console.ReadLine();
                        if (calculator.verifyIfIsNumber(readInput) == true)
                            calculator.secondNumber = Convert.ToDouble(readInput);
                        else
                            Console.WriteLine("Not a number try again");

                    }
                    readInput = "p";
                    while (calculator.verifyIfValidOperation(readInput) != 1)
                    {
                        Console.WriteLine("Choose the operation ( + , - , * , /)");
                        readInput = Console.ReadLine();
                        if (calculator.verifyIfValidOperation(readInput) != 1)
                            Console.WriteLine("Not a valid operation,try again");
                        else
                        {


                            switch (readInput)
                            {
                                case "+":
                                    operation = Operation.add;
                                    break;
                                case "-":
                                    operation = Operation.substract;
                                    break;
                                case "*":
                                    operation = Operation.multiply;
                                    break;
                                case "/":
                                    operation = Operation.divide;
                                    break;


                            }

                        }

                    }


                    lastNumber = calculator.applyOperation(calculator.firstNumber, calculator.secondNumber, operation);
                    Console.WriteLine(lastNumber);
                }
                else
                {
                    operationToRemember = readInput;
                    readInput = "s";
                    while (calculator.verifyIfIsNumber(readInput) != true)
                    {
                        Console.WriteLine("Enter the second number");
                        readInput = Console.ReadLine();
                        if (calculator.verifyIfIsNumber(readInput) == true)
                            calculator.secondNumber = Convert.ToDouble(readInput);
                        else
                            Console.WriteLine("Not a number try again");

                    }
                    readInput = "p";



                    switch (operationToRemember)
                    {
                        case "+":
                            operation = Operation.add;
                            break;
                        case "-":
                            operation = Operation.substract;
                            break;
                        case "*":
                            operation = Operation.multiply;
                            break;
                        case "/":
                            operation = Operation.divide;
                            break;






                    }
                    lastNumber = calculator.applyOperation(lastNumber, calculator.secondNumber, operation);
                    Console.WriteLine(lastNumber);
                }
                Console.WriteLine("Press a operation if you want to continue");
                readInput = Console.ReadLine();
            } while (readInput != "#");


        }
    }
}
