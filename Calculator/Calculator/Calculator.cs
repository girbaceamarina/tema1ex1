﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    enum Operation
    {
        add = '+',
        substract = '-',
        multiply = '*',
        divide = '/'
    }
    class Calculator
    {
        public double firstNumber { get; set; }
        public double secondNumber { get; set; }
        public int verifyIfValidOperation(string op)
        {
            if (op.Equals("+") || op.Equals("-") || op.Equals("/") || op.Equals("*"))
                return 1;
            else
                return 0;
        }
        public bool verifyIfIsNumber(String number)
        {
            double retNum;

            bool isNum = Double.TryParse(Convert.ToString(number), System.Globalization.NumberStyles.Any,
                System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public double applyOperation(double firstNumber, double secondNumber, Operation op)
        {
            double result = 0;
            switch (op)
            {
                case Operation.add: return firstNumber + secondNumber;
                case Operation.substract: return firstNumber - secondNumber;
                case Operation.multiply: return firstNumber * secondNumber;
                case Operation.divide:
                    try
                    {

                        result = firstNumber / secondNumber;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Can not devide by 0");

                    }

                    break;




            }
            return result;
        }
    }
}
